CONFIGURATION:

1. Copy examples/.fetchrc to your home directory (as ~/.fetchrc):

	Linux:
		/home/<username>/.fetchrc
	OSX:
		/Users/<username>/.fetchrc
	Windows:
		C:\Users\<username>\.fetchrc

2. Modify "providers" array in ~/.fetchrc to reflect your FTP resources:

	name: Unique name describing this provider. It will be printed in all logs, and can be used in assets to mark precisely from which specific provider a given asset
		should be downloaded from;

	class: Provider class, for FTP downloads (currently the only supported), use: "edu.szyrek.fetchdog.ftp.FTPFetchProvider";

	host: ftp host;

	port: ftp port number;

	username: ftp username;

	password: ftp password;

	prefix: fetchdog assets translate to a specific paths structure which ftp provider must follow; if this folder structure is not implemented at the ftp root directory
		 level, you must configure a path prefix to be prepended to all paths;

	maxConnections: defines a maximum number of concurrent connections to this provider; after saturating this value new connections will only be made after some of the
		 previous is freed;

3. Modify other parameters in ~/.fetchrc:
	localCache: path where fetchdog should keep local cached information (not artifacts like in mavens .repository, rather some metadata);
	globalThreads: maximum number of working threads spawned during a fetch job;
	batchThreads: maximum number of batch jobs running simultanously;

USAGE: 

1. To use fetchdog, simply run: 
	
	java -jar fetchdog.jar -h
	
   Above command will print current usage info. You can also make symlink to it, or create a runner script and put it in the path, for more convenient execution.
   
2. In examples/assets you will find example assets. You can copy them to your poject and modify to suit your needs. Currently supported fetchables are:
	a. edu.szyrek.fetchdog.FileFetchable
		filepath: path of the file in the provider folder structure; if provider has a prefix defined, this prefix will be prepended to this path
	b. edu.szyrek.fetchdog.assets.AssetFetchable
		filename: name of the file, with extension, as seen on ftp
		type: used to group files logically on the ftp, this is a name of root folder for all files of this type (examples: "video", "audio", "mesh", etc)
		version: version of the asset, also used to logically group files on server (see examples/ftp)
	c.edu.szyrek.fetchdog.libs.CppLibFetchable (!!!NOT YET AVAIABLE!!!)
		libname: name of the library, withouth extension or "lib" prefix
		linktype: dynamic/static
		c++: libstdc++ version if used on unix machine
		msvcp: msvcp version if used on windows (note that you must define both c++ and msvcp for lib to properly resolve both on unix and windows environments)
    
	Some other common params for all fetchables are:
		class: fetchable class, one of the above a., b. and c.
		provider: allows to "inline" a custom provider for this asset file, see custom_provider.ass in examples/assets
		target: target path, relative to .ass file- this is where fetchable file will be "dropped"

3. In examples/ftp direcory you will find a directory layout of example ftp folder structure based on the asset files from example/assets. 
