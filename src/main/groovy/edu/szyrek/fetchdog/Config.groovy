package edu.szyrek.fetchdog

import groovy.util.logging.Slf4j
import org.json.simple.JSONArray
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.util.Collection;

/**
 * Created by szyzyrek on 7/15/16.
 */
@Slf4j
public abstract class Config {
    private static JSONObject configuration;

    protected boolean valid = false

    private abstract void initialize()
    private abstract void validate()


    public boolean isValid() {
        return valid
    }

    public void loadJSONFile(String filePath) {
        def buffer = new StringBuffer()
        new File(filePath).withReader {r ->
                buffer.append(r.readLines())
        }
        loadJSON(buffer.toString())
    }

    public void loadJSON(String json) {
        def parser = new JSONParser()
        def newConfiguration = parser.parse(json)
        this.merge(newConfiguration)
        initialize()
    }

    public void merge(FetchDogConfig other) {
        for(def key: other.getKeys()) {
            if (this.isSet(key)) {
                mergeKey(other, key)
            } else {
                this.configuration.put(key, other.get(key))
            }
        }
    }

    private void mergeKey(FetchDogConfig otherConfig, String key) {
        Object otherValue = otherConfig.get(key)
        Object thisValue = this.get(key)
        try {
            if (thisValue instanceof Collection && otherValue instanceof Collection) {
                Collection<?> ourCollection = (Collection<?>) thisValue
                Collection<?> otherCollection = (Collection<?>) thisValue
                ourCollection.addAll(otherCollection)
            } else if (thisValue instanceof Collection) {
                Collection<?> ourCollection = (Collection<?>) thisValue
                ourCollection.add(otherValue)
            } else if (otherValue instanceof Collection) {
                Collection<?> otherCollection = (Collection<?>) thisValue
                otherValue.add(thisValue)
            } else {
                log.info("Overriding config for $key from ${thisValue} to ${otherValue}")
                configuration.put(key, otherValue)
            }
        }catch (Exception e) {
            log.error("Failed to merge configuration for $key, values are: ${thisValue} and ${otherValue}")
        }
    }

    public <T> void set(String key, T value) {
        configuration.put(key, value)
    }

    public <T> T get(String keyPath, defaultValue = null) {
        String path = keyPath;
        JSONObject current = configuration;

        T result

        while(!path.isEmpty()) {
            String nextComponent = path;
            if (path.contains(PATH_SEPARATOR)) {
                nextComponent = path.substring(0, path.indexOf(PATH_SEPARATOR))
                path = path.substring(path.indexOf(PATH_SEPARATOR)+1)
                if (nextComponent.matches(".*\\[[0-9]*\\]")) {
                    JSONArray array = (JSONArray) current.get(nextComponent.substring(0,nextComponent.indexOf('[')))
                    current = array.get(Integer.parseInt(nextComponent.substring(nextComponent.indexOf('[')+1,nextComponent.indexOf(']'))))
                } else {
                    current = current.get(nextComponent)
                }
            } else {
                if (nextComponent.matches(".*\\[[0-9]*\\]")) {
                    JSONArray array = (JSONArray) current.get(nextComponent.substring(0,nextComponent.indexOf('[')))
                    result = array.get(Integer.parseInt(nextComponent.substring(nextComponent.indexOf('[')+1,nextComponent.indexOf(']'))))
                    break
                } else {
                    result = current.get(nextComponent)
                    break
                }
            }
        }

        return result?:defaultValue
    }

    List<String> getKeys() {
        return configuration.keySet()
    }

}
