package edu.szyrek.fetchdog

import groovy.util.logging.Slf4j

/**
 * Created by bebebaba on 2016-06-19.
 */
@Slf4j
class DescriptorGenerator {
    public static File generateDescriptor(File sourceFile, boolean justGenerate) {
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))
        String input

        File assetFile = new File(sourceFile.getName().substring(0,sourceFile.getName().lastIndexOf("."))+".ass")

        if (assetFile.exists()) {
            log.warn("{} already exists, do you want to overwrite(y/n)?", assetFile.toString())
            input = consoleReader.readLine()
            if (!input.matches("[yY](es)?")) {
                log.error("Descriptor generation for {} interrupted by user", sourceFile.toString())
                return
            }
        }

        log.info("Generating asset descriptor for {}", sourceFile)

        String clazz = "edu.szyrek.fetchdog.assets.AssetFetchable"
        String target = sourceFile.toString()
        target = target.replace("\\","\\\\")

        String assetType = sourceFile.getName().substring(sourceFile.getName().lastIndexOf(".")+1, sourceFile.getName().size())
        if (!justGenerate) {
            println("Asset type($assetType): ")
            input = consoleReader.readLine()
            assetType = input.size() == 0 ? assetType : input
        }

        String fileName = sourceFile.getName().toString()
        if (!justGenerate) {
            println("File name($fileName): ")
            input = consoleReader.readLine()
            fileName = input.size() == 0 ? fileName : input
        }


        String version = "1.0.0"
        if (!justGenerate) {
            println("Version($version): ")
            input = consoleReader.readLine()
            version = input.size() == 0 ? version : input
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(assetFile))
        writer.write("" +
                "{\n"+
                "\t\"class\": \"$clazz\",\n"+
                "\t\"filename\": \"$fileName\",\n"+
                "\t\"type\": \"$assetType\",\n"+
                "\t\"target\": \"$target\",\n"+
                "\t\"version\": \"$version\"\n"+
                "}\n")
        writer.close()
    }
}
