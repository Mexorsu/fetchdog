package edu.szyrek.fetchdog

import groovy.util.logging.Slf4j;

import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * Created by bebebaba on 2016-07-14.
 */
@Slf4j
public class FetchDogArguments {
    public static final String USAGE_INFO = "Usage: \n"+
            "\t java -jar fetchdog.jar <action> [<action args>] input1 input2 ...\n"+
            "\t where <action>s are:\n"+
            "\t-f\t\t: fetch- fetches stuff described in inputs (either .ass files or directories containing .ass fi;es) from providers defined in ~/.fetchrc and in .ass files\n"+
            "\t-u <providerlist>\t: upload- above process reversed, based on .ass descryption looks locally for the file and sends it to providers pointed by comma separated list argument 'providerlist'\n"+
            "\t-u! <providerlist>\t: force upload- same as above, but doesn't warn if the file is already present on target provider, just uploads anyway\n"+
            "\t-g\t\t: generates asset descriptor (.ass file) for given input files\n"+
            "\t-g!\t\t: same as above but ask for nothing, just passes defaults everywhere\n"+
            "\t-s <providerlist>\t: uploads all stuff fetched in this run to all providers on comma separated list- not yet implemented\n"+
            "\t-v\t\t: verbose\n"+
            "\t-h\t\t: prints this usage info\n"

    private boolean valid = false
    private List<String> errors = {"Command arguments are not valid!"}

    public FetchDogAction action = FetchDogAction.NOOP;
    public List<String> syncTargetProviderNames
    public boolean forceUpload = false
    public boolean justGenerate = false
    public boolean syncFlag = false
    public List<Path> paths

    private FetchDogConfig targetConfig;

    public FetchDogArguments(FetchDogConfig config) {

    }

    /*
   *   Parses command arguments according to "contract" from help message,
   *   which is printed when run with none or invalid args.
   *
   *   Returns a list of filesystem paths on which given action is to performed on, sets
   *   the action to perform, and also allows to pass some limited configuration
   *   overrides, according to welcome message.
   *
   *   Changes to this method should be reflected in help/welcome message
   */
    void parseCommandLineArgs(String[] args) {
        List<String> argList = new LinkedList<>(Arrays.asList(args))
        List<Path> paths = new ArrayList<>()

        while (argList.size()>0)
        {
            String arg = argList.remove(0)
            switch (arg) {
                case "-d":
                    targetConfig.set(FetchDogConfig.DEBUG, true)
                    break
                case "-g":
                    targetConfig.set()
                    setAction(FetchDogAction.GEN_DESC)
                    break
                case "-G":
                    parseGitignoreArg()
                    break
                case "-g!":
                    setAction(FetchDogAction.GEN_DESC)
                    justGenerate = true
                    break
                case "-h":
                    println USAGE_INFO
                    break
                case "-v":
                    targetConfig.setVerbose(true);
                    break
                case "-f":
                    setAction(FetchDogAction.FETCH)
                    break
                case "-s":
                    parseSyncTargets(argList);
                    break
                case "-u":
                    setAction(FetchDogAction.UPLOAD)
                    try {
                        syncTargetProviderNames = Arrays.asList(argList.remove(0).split(","))
                        for (def provName: syncTargetProviderNames) {
                            if (!FetchDogConfig.getProviders().containsKey(provName)) {
                                errors.add("Provider $provName not found, check your ~/.fetchrc or command line arguments")
                            }
                        }
                    }catch (IndexOutOfBoundsException e) {
                        errors.add("-u option must be followed by target providers list")
                    }
                    break
                case "-u!":
                    setAction(FetchDogAction.UPLOAD)
                    forceUpload = true
                    try {
                        syncTargetProviderNames = Arrays.asList(argList.remove(0).split(","))
                        for (def provName: syncTargetProviderNames) {
                            if (!targetConfig.getProviders().containsKey(provName)) {
                                errors.add("Provider $provName not found, check your ~/.fetchrc or command line arguments")
                            }
                        }
                    }catch (IndexOutOfBoundsException e) {
                        errors.add("-u! option must be followed by target providers list")
                    }
                    break
                case "-i":
                    targetConfig.set(FetchDogConfig.LABEL_INSULTS, true)
                    break
                default:
                    paths.add(Paths.get(arg))
                    break
            }
        }
        if (paths.size()==0) {
            errors.add("Must supply either .ass file path or directory path (for batch fetch) as an argument")
        }
        if (errors.empty) {
            valid = true
        } else {
            errors.forEach{e -> println(e)}
            println(USAGE_INFO)
            valid = false
        }
    }

    private void parseSyncTargets(List<String> argList) {
        syncFlag = true
        try {
            syncTargetProviderNames = Arrays.asList(argList.remove(0).split(","))
            for (def provName: syncTargetProviderNames) {
                if (!targetConfig.getProviders().containsKey(provName)) {
                    errors.add("Provider $provName not found, check your ~/.fetchrc or command line arguments")

                }
            }
        }catch (IndexOutOfBoundsException e) {
            errors.add("-s option must be followed by target providers list")
        }
    }

    private void parseGitignoreArg() {
        try{
            targetConfig.setGitignoreLocation(Paths.get(argList.remove(0)))
        }catch (IndexOutOfBoundsException e) {
            errors.add("-G option must be followed by a valid .gitignore file path")
        }
    }

    private void setAction(FetchDogAction act) {
        if (action!=FetchDogAction.NOOP) {
            errors.add("You can choose only one action per run")
        } else {
            action = act
            targetConfig.set()
        }
    }

}
