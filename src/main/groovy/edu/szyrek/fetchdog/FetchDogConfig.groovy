package edu.szyrek.fetchdog

import groovy.util.logging.Slf4j
import org.json.simple.JSONArray
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser

import java.nio.file.Path
import java.nio.file.Paths;

@Slf4j
class FetchDogConfig extends Config {
    public final static String LABEL_PROVIDERS = "providers"
    public final static String LABEL_INSULTS = "insults"
    public final static String LABEL_MAX_GLOBAL_THREADS = "globalThreads"
    public final static String LABEL_MAX_BATCH_THREADS = "batchThreads"
    public final static String LABEL_LOCAL_CACHE_PATH = "localCache"
    public final static String LABEL_GITIGNORE_LOCATION = "gitignoreLocation"
    public final static String LABEL_VERBOSE = "verbose"

    public final static String DEBUG = "debug"

    private final static String DEFAULT_LOCAL_CACHE_PATH = System.getProperty("user.home")+File.separator+".fetchdog"
    private final static String DEFAULT_CONFIG_FILE_LOCATION = System.getProperty("user.home") + File.separator + ".fetchrc"
    private final static String PATH_SEPARATOR = "."

    private static JSONObject configuration;
    private static Map<String,FetchProvider> providers;
    private static Path localCachePath;

    public static final FetchDogConfig MAIN = getMainConfig()

    private static FetchDogConfig getMainConfig() {
        MainConfigInstanceHolder.instance
    }

    private static final class MainConfigInstanceHolder {
        public static final FetchDogConfig instance
        static {
            instance = new FetchDogConfig()
            instance.loadJSONFile(DEFAULT_CONFIG_FILE_LOCATION)
        }
    }

    @Override
    private void initialize() {
        def providers = new HashMap<>()
        JSONArray providersInfo = this.get(LABEL_PROVIDERS)

        for (JSONObject providerInfo: providersInfo) {
            providers.put(providerInfo.get(FetchProvider.LABEL_PROVIDER_NAME), FetchProvider.fromJSON(providerInfo))
        }

        localCachePath = configuration.get(LABEL_LOCAL_CACHE_PATH) == null ?
                Paths.get(DEFAULT_LOCAL_CACHE_PATH) : Paths.get(configuration.get(LABEL_LOCAL_CACHE_PATH))
        validate()
    }

    @Override
    private void validate() {
        valid = true
        //TODO: temporary not much validation, make more ;)
        if ((int)configuration.get(LABEL_MAX_GLOBAL_THREADS)<2) {
            log.error("At least 2 threads required, check 'maxThreads' in your ~/.fetchrc")
            valid = false
        }
        if (!isValid()) {
            throw new IllegalStateException("Configuration is invalid!")
        }
    }

    void setGitignoreLocation(Path location) {
        this.configuration.put(LABEL_GITIGNORE_LOCATION, location)
    }

    boolean isSet(String key) {
        return configuration.containsKey(key)
    }

    void setVerbose() {
        this.configuration.put(LABEL_VERBOSE, true)
    }

    Path getLocalCachePath() {
        return localCachePath
    }

    Map<String, FetchProvider> getProviders() {
        return this.providers
    }



}