package edu.szyrek.fetchdog

import com.google.common.io.Files
import edu.szyrek.fetchdog.cache.LocalCache
import org.json.simple.JSONArray
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.nio.file.Path
import java.util.concurrent.Callable
import groovy.util.logging.Slf4j

/**
 * Created by bebebaba on 2016-06-05.
 */
@Slf4j
public class FetchJob implements Callable {
    Path jobfilePath;
    List<FetchProvider> providers
    boolean sync = false;
    FetchProvider syncTarget
    boolean fetched = false

    FetchJob(Path filePath, FetchProvider targetProvider, List<FetchProvider> providers = FetchDogConfig.getProviders().values()) {
        this.jobfilePath = filePath;
        this.providers = providers
        this.sync = true
        this.syncTarget = targetProvider
    }

    FetchJob(Path filePath, List<FetchProvider> providers) {
        this.jobfilePath = filePath
        this.providers = providers
    }

    FetchJob(Path filePath) {
        providers = new LinkedList<>()
        providers.addAll(FetchDogConfig.getProviders().values())
        this.jobfilePath = filePath
    }

    private void fetchOne(JSONObject fetchableBP) {
        Class<?> fetchableClass = Class.forName((String)fetchableBP.get(Fetchable.LABEL_CLASS))
        Fetchable fetchable = fetchableClass.newInstance(fetchableBP)
        fetchable.isValid()
        String finalTargetPath = jobfilePath.getParent().toString()+File.separator+fetchable.getTargetPath().toString()
        List<FetchProvider> providersToUse = new LinkedList<>()
        if (fetchable.getProvider() != null) {
            providersToUse.add(fetchable.getProvider())
        }
        providersToUse.addAll(providers)
        File file = LocalCache.get(fetchable)
        if (file != null && file.exists()) {
            Date newestVersionTimestamp = new Date(0)
            for (def provider : providersToUse) {
                try {
                    provider.lock()
                    Date timestamp = provider.getLastUpdated(fetchable)
                    provider.unlock()
                    if (timestamp!=null&&timestamp.after(newestVersionTimestamp)) {
                        newestVersionTimestamp = timestamp;
                    }
                } catch (FetchException e) {
                    StringWriter sw = new StringWriter()
                    PrintWriter pw = new PrintWriter(sw)
                    e.printStackTrace(pw)
                    log.warn "EXCEPTION: ${e.getMessage()} ${sw.toString()}"
                }
            }
            if (newestVersionTimestamp.compareTo(new Date(file.lastModified())) <= 0)
            {
                if (file.toString().equals(finalTargetPath)) {
                    log.info "$finalTargetPath alread resolved"
                    fetched = true
                    return
                } else if (!(new File(finalTargetPath)).exists()) {
                    log.info "Copying ${file.toString()} to $finalTargetPath"
                    Files.copy(file, new File(finalTargetPath))
                    fetched = true
                    return
                } else {
                    log.info "$finalTargetPath alread resolved"
                    fetched = true
                    return
                }
            }
        }
        File artifact
        Iterator<FetchProvider> providerIterator = providersToUse.iterator()

        while (!fetched) {
            if (providerIterator.hasNext()) {
                FetchProvider provider = providerIterator.next()
                try {
                    boolean canTryDownload = provider.tryLock()
                    if (canTryDownload) {
                        artifact = provider.fetch(fetchable, jobfilePath.getParent())
                        fetched = true
                        provider.unlock()
                    } else {
                        log.trace "cant download from ${provider.getDescription()}, it's already used"
                    }
                } catch (FetchException e) {
                    providerIterator.remove()
                    log.warn "${e.getMessage()}"
                }
            } else {
                providerIterator = providersToUse.iterator()
                if (!providerIterator.hasNext())
                {
                    throw new FetchException("${fetchable.getDescription()} not found on any of the providers")
                }
                else
                {
                    sleep(2000)
                }
            }
        }

        LocalCache.put(fetchable, artifact)
    }

    void run() {
        JSONParser parser = new JSONParser()
        StringBuffer buffer = new StringBuffer()
        new File(jobfilePath.toAbsolutePath().toString()).withReader { r ->
            buffer.append(r.readLines())
        }

        Object jobObject = parser.parse(buffer.toString())


        if (jobObject instanceof JSONArray) {
            JSONArray jobDesc = (JSONArray) jobObject
            for (def object: jobDesc) {
                fetchOne(object)
            }
        } else if (jobObject instanceof  JSONObject) {
            JSONObject jobDesc = (JSONObject) jobObject
            fetchOne(jobDesc)
        }
    }

    Object call() throws Exception {
        run()
        return fetched
    }
}
