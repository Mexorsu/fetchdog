package edu.szyrek.fetchdog

import org.json.simple.JSONObject

import java.nio.file.Path
import java.util.concurrent.locks.Lock

class FetchException extends Exception {
    Exception wrapped
    String msg

    FetchException(Exception e) {
        this.wrapped = e
    }
    FetchException(String msg) {
        this.wrapped = null;
        this.msg = msg
    }
    @Override
    String getMessage() {
        //StringWriter sw = new StringWriter();
        //wrapped.printStackTrace(new PrintWriter(sw))
        if (this.wrapped==null) {
            return "Fetch failed because of $msg"
        }
        String cause;
        if (wrapped.getMessage()!=null) {
            cause = wrapped.getMessage()
        } else {
            cause = wrapped.getClass().getCanonicalName()
        }
        if (cause.startsWith("Fetch failed because of ")) {
            return "$cause"
        } else {
            return "Fetch failed because of  $cause"
        }
    }

}

abstract class FetchProvider implements Serializable {
    public static final long serialVersionUID = -7588980448693010398L;
    public static final String LABEL_PROVIDER_CLASS = "class"
    public static final String LABEL_PROVIDER_NAME = "name"

    String name

    abstract void init(JSONObject config)
    abstract File fetch(Fetchable stuff, Path targetDir) throws FetchException
    abstract void upload(Fetchable stuff, Path fetchDir) throws FetchException
    abstract Date getLastUpdated(Fetchable stuff)
    abstract String getDescription();

    class LockHolder implements Serializable {
        public static final long serialVersionUID = -7588380438693010398L;
        boolean locked
    }
    LockHolder mutex = new LockHolder()

    static FetchProvider fromJSON(JSONObject config){
        Class<?> providerClass = Class.forName(config.get(LABEL_PROVIDER_CLASS))

        FetchProvider provider = providerClass.newInstance()
        provider.init(config)
        provider.name = config.get(LABEL_PROVIDER_NAME, "Unnamed Provider")
        return provider
    }

    boolean isLocked() {
        return mutex.isLocked()
    }

    boolean tryLock() {
        synchronized (mutex) {
            if (mutex.isLocked())
            {
                return false
            }
            else
            {
                lock()
                return true
            }
        }
    }

    void lock() {
        synchronized (mutex) {
            if(mutex.isLocked()) {
                mutex.wait()
            }
            mutex.setLocked(true)
        }
    }

    void unlock() {
        synchronized (mutex) {
            mutex.setLocked(false)
            mutex.notify()
        }
    }
}