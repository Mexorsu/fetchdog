package edu.szyrek.fetchdog

import com.sun.scenario.effect.impl.prism.PrEffectHelper
import org.json.simple.JSONObject

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

abstract class Fetchable implements Serializable {
    public static final String LABEL_TARGET = "target"
    public static final String LABEL_PROVIDER = "provider"
    public static final String LABEL_CLASS = "class"
    static final long serialVersionUID = -7588980448693010399L;
    String targetPath
    FetchProvider provider
    Class<?> clazz

    abstract String getDescription()

    Fetchable(){throw IllegalStateException("Cannot instantiate fetchable without JSONObject!")}

    Fetchable(JSONObject descriptor) {
        this.targetPath = descriptor.get(LABEL_TARGET)
        this.provider = descriptor.get(LABEL_PROVIDER) != null ? FetchProvider.fromJSON(descriptor.get(LABEL_PROVIDER)) : null
        this.clazz = Class.forName(descriptor.get(LABEL_CLASS))
    }

    boolean shouldUpdate() {
        return true
    }


    boolean isValid() {
        if (clazz==null)
            throw new ValidationException("'$LABEL_CLASS'", "cannot be null")
        if (targetPath==null)
            throw new ValidationException("'$LABEL_TARGET'", "cannot be null")
    }

    public Path getTargetPath() {
        return targetPath!=null?Paths.get(this.targetPath):null
    }

    public void setTargetPath(Path path){
        this.targetPath = path.toString()
    }

    @Override
    boolean equals(Object obj) {
        if (obj != null && obj instanceof Fetchable) {
            Fetchable other = (Fetchable) obj
            return Objects.equals(this.targetPath, other.getTargetPath()) &&
                    Objects.equals(this.provider, other.getProvider()) &&
                    Objects.equals(this.clazz, other.getClazz())
        } else {
            return false
        }
    }

    @Override
    int hashCode() {
       return Objects.hash(targetPath, provider)//, clazz)
    }

}