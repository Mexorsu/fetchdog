package edu.szyrek.fetchdog;

import edu.szyrek.fetchdog.Fetchable;
import org.json.simple.JSONObject;

import java.nio.file.Path
import java.nio.file.Paths;

/**
 * Created by bebebaba on 2016-06-05.
 */
class FileFetchable extends Fetchable {
    public static final long serialVersionUID = -7588980448693010396L;
    public static final String LABEL_FILE_PATH = "filepath";
    String filePath;

    private FileFetchable(){}

    String getDescription() {
        return this.filePath
    }

    FileFetchable(JSONObject descriptor) {
        super(descriptor);
        this.filePath = descriptor.get(LABEL_FILE_PATH)
    }
    boolean isValid() {
        super.isValid()
        if (filePath==null)
            throw new ValidationException("'$LABEL_FILE_PATH'", "path cannot be null")
    }

    @Override
    boolean equals(Object obj) {
        if ((obj != null)&&(obj instanceof FileFetchable)) {
            FileFetchable other = (FileFetchable) obj
            return Objects.equals(this.filePath, other.getFilePath())
        } else {
            return false
        }
    }

    @Override
    int hashCode() {
        return Objects.hash(filePath)
    }
}
