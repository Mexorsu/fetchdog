package edu.szyrek.fetchdog

import edu.szyrek.fetchdog.batch.FetchBatchJob
import edu.szyrek.fetchdog.batch.UploadBatchJob
import groovy.util.logging.Slf4j

import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.ExecutionException
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future


enum FetchDogAction {
    FETCH, BATCH, UPLOAD, CLEAN, NOOP, GEN_DESC
}

@Slf4j
class GroovyEntryPoint {


    private static ExecutorService globalThreadPool
    private static ExecutorService batchThreadPool

    public static getGlobalThreadPool() {
        return globalThreadPool
    }

    static void main(String[] args) {
        FetchDogArguments arguments = new FetchDogArguments(FetchDogConfig.MAIN)
        arguments.parseCommandLineArgs(args)

        int globalThreadPoolSize = (int)FetchDogConfig.MAIN.get(FetchDogConfig.LABEL_MAX_GLOBAL_THREADS)
        int batchThreadPoolSize = (int)FetchDogConfig.MAIN.get(FetchDogConfig.LABEL_MAX_BATCH_THREADS)

        globalThreadPool = Executors.newFixedThreadPool(globalThreadPoolSize)
        batchThreadPool = Executors.newFixedThreadPool(batchThreadPoolSize)

        switch (arguments.action) {
            case FetchDogAction.FETCH:
                fetch(arguments.paths)
                break
            case FetchDogAction.GEN_DESC:
                geterateDescriptors(arguments.paths)
                break
            case FetchDogAction.UPLOAD:
                upload(arguments.paths, arguments.forceUpload)
                break
            default:
                log.error("Action ${arguments.action} not implemented")
                println arguments.USAGE_INFO
                break
        }
        globalThreadPool.shutdown()
        batchThreadPool.shutdown()
    }





    static void geterateDescriptors(List<Path> paths) {
        for (Path path: paths) {
            if (Files.isRegularFile(path)) {
                DescriptorGenerator.generateDescriptor(new File(path.toString()), justGenerate)
            } else {
                log.error("{} is not a regular file... skipping",path)
            }
        }
    }

    static void upload(List<Path> paths, boolean force) {
        List<Future<Boolean>> results = new ArrayList<>();
        for (Path path : paths) {
            if (Files.isDirectory(path)) {
                try {
                    UploadBatchJob uploadBatch = new UploadBatchJob(path, targetProviderNames, force)
                    results.add(batchThreadPool.submit(uploadBatch))
                } catch (Exception e) {
                    log.error("{}",e.getMessage())
                    e.printStackTrace()
                }
            } else if (Files.isRegularFile(path)) {
                try {
                    UploadJob uploadJob = new UploadJob(path, targetProviderNames, force)
                    results.add(globalThreadPool.submit(uploadJob))
                } catch (Exception e) {
                    log.error("{}",e.getMessage())
                    e.printStackTrace()
                }
            }
        }
        boolean gotSomeErrs = false
        for (def result: results) {
            try {
                if (result != null && result.get()!=true) {
                    gotSomeErrs = true
                }
            } catch (Exception e) {
                log.error(e.getMessage())
                gotSomeErrs = true
            }
        }
        if (gotSomeErrs) {
            log.error("Some tasks failed, see previous logs")
        } else {
            log.info("Upload job completed successfully")
        }
    }

    static void fetch(List<Path> paths) {
        List<Future<Boolean>> results = new ArrayList<>();

        for (Path path : paths) {
            if (Files.isDirectory(path)) {
                try {
                    FetchBatchJob batch
                    if (syncFlag) {
                        List<FetchProvider> syncTargets = new ArrayList<>()
                        for (def provName: targetProviderNames) {
                            syncTargets.add(FetchDogConfig.getProviders().get(provName))
                        }
                        batch = new FetchBatchJob(path, syncTargets)
                    } else {
                        batch = new FetchBatchJob(path)
                    }
                    results.add(batchThreadPool.submit(batch))
                }catch (Exception e) {
                    log.error("{}", e.getMessage())
                    e.printStackTrace()
                }
            } else if (Files.isRegularFile(path)) {
                FetchJob job
                if (syncFlag) {
                    List<FetchProvider> syncTargets = new ArrayList<>()
                    for (def provName: targetProviderNames) {
                        syncTargets.add(FetchDogConfig.getProviders().get(provName))
                    }
                    job = new FetchJob(path, syncTargets)
                } else {
                    job = new FetchJob(path)
                }
                try {
                    results.add(globalThreadPool.submit(job))
                } catch (Exception e) {
                    log.error("{}",e.getMessage())
                    e.printStackTrace()
                }

            } else {
                log.error("{} is neither directory of a file", path)
                println USAGE_INFO
            }
        }
        boolean gotSomeErrs = false
        for (def result: results) {
            try {
                if (result != null && result.get() != true) {
                    gotSomeErrs = true
                }
            } catch (ExecutionException e) {
                log.error(e.getMessage())
                gotSomeErrs = true
            }
        }
        if (gotSomeErrs) {
            log.error("Some tasks failed, see previous logs")
        } else {
            log.info("Fetch job completed successfully")
        }
    }
}