package edu.szyrek.fetchdog

import groovy.util.logging.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser

import java.nio.file.Path
import java.util.concurrent.Callable;

/**
 * Created by bebebaba on 2016-06-19.
 */
@Slf4j
public class UploadJob implements Callable {
    Path jobfilePath
    List<FetchProvider> targets
    Map<String,Boolean> succeeded = new HashMap<>()
    boolean forceUpload
    static Object userInputMutex = new Object()

    UploadJob(Path filePath) {
        this.jobfilePath = filePath
        this.targets = new ArrayList<>()
    }

    UploadJob(Path filePath, List<String> targetProviderNames, boolean force = false) {
        this.jobfilePath = filePath
        this.targets = new ArrayList<>()
        this.forceUpload = force
        for (def provName: targetProviderNames) {
            targets.add(FetchDogConfig.getProviders().get(provName))
        }
        if (targets.size()==0) {
            throw new IllegalStateException("No providers found! Check your ~/.fetchrc")
        }
    }

    private boolean allDone(List<FetchProvider> usedProviders) {
        for (def provider: usedProviders) {
            Boolean res = succeeded.get(provider.getName())
            if (res == null || res.equals(false)) {
                return false
            }
        }
        return true
    }

    private void uploadOne(JSONObject fetchableBP) {
        Class<?> fetchableClass = Class.forName((String)fetchableBP.get(Fetchable.LABEL_CLASS))
        Fetchable fetchable = fetchableClass.newInstance(fetchableBP)
        fetchable.isValid()
        String finalTargetPath = jobfilePath.getParent().toString()+ File.separator+fetchable.getTargetPath().toString()
        List<FetchProvider> providersToUse = new LinkedList<>()
        if (fetchable.getProvider() != null) {
            providersToUse.add(fetchable.getProvider())
        }
        providersToUse.addAll(targets)

        Iterator<FetchProvider> providersIterator = providersToUse.iterator()

        while (!allDone(providersToUse)){
            if (providersIterator.hasNext()) {
                def provider = providersIterator.next()
                try {
                    provider.lock()
                    Date last = provider.getLastUpdated(fetchable)
                    if (last!=null) {
                        if (forceUpload) {
                            log.warn("{} already exists on {}, overwriting with -u! option", fetchable.getDescription(), provider.getDescription())
                        } else {
                            synchronized (userInputMutex) {
                                log.warn("{} already exists on {}, are you sure you want to continue?", fetchable.getDescription(), provider.getDescription())
                                BufferedReader reader = new BufferedReader(new InputStreamReader(
                                        System.in));
                                String input = reader.readLine()
                                if (!input.matches("[yY](es)?")) {
                                    log.error("Uploading of {} to {} interrupted by user", fetchable.getDescription(), provider.getDescription())
                                    provider.unlock()
                                    providersIterator.remove()
                                    continue
                                }
                            }
                        }
                    }
                    provider.upload(fetchable, jobfilePath.getParent())
                    provider.unlock()
                    succeeded.put(provider.getName(), true)
                } catch (Exception ex) {
                    log.warn("Failed to upload ${fetchable.getDescription()} to ${provider.getDescription()} because of ${ex.getMessage()}")
                }
            } else {
                providersIterator = providersToUse.iterator()
                sleep(2000)
            }
        }
    }

    void run() {
        JSONParser parser = new JSONParser()
        StringBuffer buffer = new StringBuffer()
        new File(jobfilePath.toAbsolutePath().toString()).withReader { r ->
                buffer.append(r.readLines())
        }

        Object jobObject = parser.parse(buffer.toString())


        if (jobObject instanceof JSONArray) {
            JSONArray jobDesc = (JSONArray) jobObject
            for (def object: jobDesc) {
                uploadOne(object)
            }
        } else if (jobObject instanceof  JSONObject) {
            JSONObject jobDesc = (JSONObject) jobObject
            uploadOne(jobDesc)
        }
    }

    Object call() throws Exception {
        run()
        for (def key: succeeded.keySet()) {
            if (succeeded.get(key)==false) {
                return false
            } else {
                return true
            }
        }
    }
}
