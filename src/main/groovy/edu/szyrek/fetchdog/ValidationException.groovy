package edu.szyrek.fetchdog

/**
 * Created by bebebaba on 2016-06-05.
 */
class ValidationException extends Exception {
    String whatNV;
    String whyNV;
    public ValidationException(String whatNV, String whyNV) {
        this.whatNV = whatNV;
        this.whyNV = whyNV;
    }
    @Override
    public String getMessage() {
        return "$whatNV is not valid, because $whyNV"
    }
}
