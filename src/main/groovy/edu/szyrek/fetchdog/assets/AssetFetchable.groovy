package edu.szyrek.fetchdog.assets

import edu.szyrek.fetchdog.Fetchable
import edu.szyrek.fetchdog.FileFetchable
import edu.szyrek.fetchdog.ValidationException
import org.json.simple.JSONObject

import java.nio.file.Paths

/**
 * Created by bebebaba on 2016-06-05.
 */
class AssetFetchable extends FileFetchable {
    public static final long serialVersionUID = -7588980448693010395L;
    public static final String LABEL_ASSET_FILENAME = "filename"
    public static final String LABEL_ASSET_VERSION = "version"
    public static final String LABEL_ASSET_TYPE = "type"

    String name;
    String version;
    String type;

    AssetFetchable(JSONObject description) {
        super(description)
        this.name = description.get(LABEL_ASSET_FILENAME)
        this.version = description.get(LABEL_ASSET_VERSION)
        this.type = description.get(LABEL_ASSET_TYPE)
        if (filePath == null && name != null) {
            String justname = (name.substring(0, name.indexOf('.')))
            this.filePath = this.type + '/' +
                            justname + '/' +
                            this.version + '/' +
                            this.name
            if (targetPath == null) {
                this.targetPath = Paths.get(this.name)
            }
        }
    }
    boolean isValid() {
        super.isValid()
        if (name==null)
            throw new ValidationException("'$LABEL_ASSET_FILENAME'", "cannot be null")
        if (version==null)
            throw new ValidationException("'$LABEL_ASSET_VERSION'", "cannot be null")
        if (type==null)
            throw new ValidationException("'$LABEL_ASSET_TYPE'", "cannot be null")
    }

    @Override
    boolean equals(Object obj) {
        if (obj != null && obj instanceof AssetFetchable) {
            AssetFetchable other = (AssetFetchable) obj
            return Objects.equals(this.name, other.getName())&&
                    Objects.equals(this.version, other.getVersion()) &&
                    Objects.equals(this.type, other.getType())
        } else {
            return false
        }
    }

    @Override
    int hashCode() {
        return Objects.hash(name, version, type)
    }
}
