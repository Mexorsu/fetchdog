package edu.szyrek.fetchdog.assets.libs

import edu.szyrek.fetchdog.FileFetchable
import edu.szyrek.fetchdog.ValidationException
import edu.szyrek.fetchdog.system.Arch
import edu.szyrek.fetchdog.system.OperationSystem
import org.json.simple.JSONObject

/**
 * Created by szyzyrek on 6/12/16.
 */
class CppLibFetchable extends FileFetchable {
    public final static String LABEL_LIB_NAME = "libname"
    public final static String LABEL_LINK_TYPE = "linktype"
    public final static String LABEL_STD_LIB = "c++"
    public final static String LABEL_MSVC= "msvcp"
    public final static String LABEL_ARCH = "arch"
    public final static String LABEL_VERSION = "version"
    public final static String LABEL_OS = "system"

    String libName
    String linkType
    String os
    String arch
    String version
    String stdLibVersion

    CppLibFetchable(JSONObject descriptor) {
        super(descriptor)
        this.libName = descriptor.get(LABEL_LIB_NAME)
        this.linkType = descriptor.get(LABEL_LINK_TYPE, "static")
        this.arch = descriptor.get(LABEL_ARCH, Arch.arch.toString())
        this.version = descriptor.get(LABEL_VERSION)
        this.os = descriptor.get(LABEL_OS, Arch.os.toString())

        String extension = ""
        String prefix = ""
        if (Arch.os == OperationSystem.WINDOWS) {
            prefix = ""
            stdLibVersion = descriptor.get(LABEL_MSVC)
            if (linkType.equals("static")) {
                extension = ".lib"
            } else {
                extension = ".dll"
            }
        } else {
            prefix = "lib"
            stdLibVersion = descriptor.get(LABEL_STD_LIB)
            if (linkType.equals("static")) {
                extension = ".a"
            } else {
                extension = ".so"
            }
        }

        filePath = "libs" +
                File.separator+
                libName+
                File.separator+
                version+
                File.separator+
                os+arch+
                File.separator+
                stdLibVersion+
                File.separator+
                prefix+libName+"."+(extension)
    }
    boolean isValid() {
        super.isValid()
        if (version==null||version=="")
            throw new ValidationException("'$LABEL_VERSION' cannot be empty")
        if (libName==null||libName=="")
            throw new ValidationException("'$LABEL_LIB_NAME'", "cannot be null")
        if (stdLibVersion==null||stdLibVersion=="")
            if (Arch.os==OperationSystem.WINDOWS) {
                throw new ValidationException("'$LABEL_MSVC'", "cannot be null")
            } else {
                throw new ValidationException("'$LABEL_STD_LIB'", "cannot be null")
            }
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false
        if (!super.equals(o)) return false

        CppLibFetchable that = (CppLibFetchable) o

        if (arch != that.arch) return false
        if (libName != that.libName) return false
        if (linkType != that.linkType) return false
        if (stdLibVersion != that.stdLibVersion) return false

        return true
    }

    int hashCode() {
        int result = super.hashCode()
        result = 31 * result + (libName != null ? libName.hashCode() : 0)
        result = 31 * result + (linkType != null ? linkType.hashCode() : 0)
        result = 31 * result + (arch != null ? arch.hashCode() : 0)
        result = 31 * result + (stdLibVersion != null ? stdLibVersion.hashCode() : 0)
        return result
    }

}
