package edu.szyrek.fetchdog.batch

import edu.szyrek.fetchdog.FetchJob
import edu.szyrek.fetchdog.FetchProvider
import edu.szyrek.fetchdog.GroovyEntryPoint
import groovy.util.logging.Slf4j

import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Future

import static groovy.io.FileType.FILES


/**
 * Created by bebebaba on 2016-06-05.
 */
class FetchBatchJobException extends RuntimeException {
    FetchBatchJobException(String msg) {
        super(msg)
    }
}

@Slf4j
class FetchBatchJob implements Callable {
    ExecutorService executor
    Path rootDir
    boolean sync = false
    List<FetchProvider> targetProviders
    boolean succeeded = false


    public FetchBatchJob(Path rootDir) {
        this(rootDir, new ArrayList<>())
    }

    public FetchBatchJob(Path rootDir, List<FetchProvider> targetProviders) {
        this.rootDir = rootDir
        this.sync = true
        this.targetProviders = targetProviders
    }

    void run() {
        log.info("Starting batch fetch job in {}", rootDir.toString())
        executor = GroovyEntryPoint.getGlobalThreadPool() //Executors.newFixedThreadPool((int)FetchDogConfig.get(FetchDogConfig.LABEL_MAX_GLOBAL_THREADS, 10))
        List<Future<Boolean>> results = new ArrayList<>();

        new File(rootDir.toString()).eachFileRecurse(FILES) {
            if(it.name.endsWith('.ass')) {
                log.info("Starting fetch job for {}", it.name)
                String jobFilePath = it.path
                if (sync) {
                    results.add(executor.submit(new FetchJob(Paths.get(jobFilePath)), targetProvider))
                } else {
                    results.add(executor.submit(new FetchJob(Paths.get(jobFilePath))))
                }
            }
        }
        for (def result: results) {
            if (result != null && result.get()!=true) {
                log.error("Batch fetch job in {} failed", rootDir)
                throw new FetchBatchJobException("ERROR: Some of the fetch jobs failed, for details check logs above")
            }
        }
        log.info("Batch fetch job in {} succeeded", rootDir.toString())
        succeeded = true
    }

    Object call() throws Exception {
            run()
            return succeeded
    }
}
