package edu.szyrek.fetchdog.cache

import edu.szyrek.fetchdog.FetchDogConfig
import edu.szyrek.fetchdog.Fetchable
import groovy.util.logging.Slf4j

import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.ConcurrentHashMap

/**
 * Created by bebebaba on 2016-06-05.
 */
@Slf4j
class LocalCache {
    static Map<Fetchable, File> cache
    static File localCacheDir
    static File cachemap

    static {
        localCacheDir = new File(FetchDogConfig.getLocalCachePath().toString())
        cache = new ConcurrentHashMap<>()
        if (!localCacheDir.exists()) {
            log.warn("No directory at {}, creating", localCacheDir)
            Files.createDirectories(Paths.get(localCacheDir.toString()))
        }
        cachemap = new File(localCacheDir.toString()+File.separator+".cachemap")

        if (!cachemap.exists()) {
            log.warn("No cache data at {}/.cachemap, initializing local cache", localCacheDir)
            Files.createFile(Paths.get(cachemap.toString()))
        } else {
            try {
                cachemap.withObjectInputStream {i->
                    cache = i.readObject()
                }
                List<Fetchable> toRemove = new ArrayList<>()
                for (def entry: cache) {
                    if (!entry.getValue().exists()) {
                        toRemove.add(entry.getKey())
                    }
                }
                for (def entry: toRemove) {
                    cache.remove(entry)
                }
            } catch (Exception e) {
                log.error("failed to initiallize cache, reason: ${e.getMessage()}")
            }

        }

    }

    static File get(Fetchable key) {
        return cache.get(key)
    }

    static void put(Fetchable descriptor, File artifact) {
        ObjectOutputStream oos
        try {
            this.cache.put(descriptor, artifact)
            oos = new ObjectOutputStream(new FileOutputStream(cachemap, false))
            oos.writeObject(cache)
        }catch (Exception e) {
            log.error("Failed to write ${descriptor.getTargetPath()} to cache, reason: ${e.getMessage()}")
        }finally{
            if (oos!=null){
                try {
                    oos.close()
                }catch (Exception e) {
                    log.error("failed to close FileOutputStream on file: ${cachemap.toString()}")
                }
            }
        }
    }

}
