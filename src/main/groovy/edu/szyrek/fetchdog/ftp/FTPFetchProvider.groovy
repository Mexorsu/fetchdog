package edu.szyrek.fetchdog.ftp

import edu.szyrek.fetchdog.FetchException
import edu.szyrek.fetchdog.FetchProvider
import edu.szyrek.fetchdog.Fetchable
import edu.szyrek.fetchdog.FileFetchable
import org.apache.commons.net.ftp.FTPClient
import org.json.simple.JSONObject

import java.nio.file.Path
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Semaphore

class FTPFetchProvider extends FetchProvider {
    public static final long serialVersionUID = -7588980448693010397L;
    public static final String LABEL_HOST = "host"
    public static final String LABEL_PORT = "port"
    public static final String LABEL_USERNAME = "username"
    public static final String LABEL_PASSWORD = "password"
    public static final String LABEL_PATH_PREFIX = "prefix"
    public static final String LABEL_MAX_CONNECTIONS = "maxConnections"
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss")

    private String host;
    private int port;
    private String username;
    private String password;
    private String pathPrefix;
    private Semaphore lock;
    transient FTPHelper ftp;

    private int maxConnections;

    void init(JSONObject config) {
        this.host = config.get(LABEL_HOST, "localhost")
        this.port = config.get(LABEL_PORT, "21")
        this.username = config.get(LABEL_USERNAME, "")
        this.password = config.get(LABEL_PASSWORD, "")
        this.pathPrefix = config.get(LABEL_PATH_PREFIX)
        this.maxConnections = (int)config.get(LABEL_MAX_CONNECTIONS, 3)
        ftp = new FTPHelper(host, username, password, port)
        this.lock = new Semaphore(this.maxConnections)
    }

    File fetch(Fetchable stuff, Path targetDir) throws FetchException {
        if (!stuff instanceof  FileFetchable) {
            throw new FetchException(new IllegalArgumentException("$stuff is not FileFetchable!"))
        } else {
            try {
                FileFetchable ftpStuff = (FileFetchable) stuff
                String targetPath = targetDir.toAbsolutePath().toString() + File.separator + stuff.getTargetPath()
                String prefixedPath
                if (pathPrefix!=null&&pathPrefix.size()>0&&pathPrefix!="null") {
                    prefixedPath = pathPrefix + '/' + ftpStuff.getFilePath()
                } else {
                    prefixedPath = ftpStuff.getFilePath()
                }
                ftp.downloadFile(Paths.get(targetPath), prefixedPath)
                return new File(Paths.get(targetPath).toAbsolutePath().toString())
            } catch (Exception e) {
                throw new FetchException(e)
            }
        }
    }

    void upload(Fetchable stuff, Path fetchDir) throws FetchException {
        if (!stuff instanceof  FileFetchable) {
            throw new FetchException(new IllegalArgumentException("$stuff is not FileFetchable!"))
        } else {
            try {
                FileFetchable ftpStuff = (FileFetchable) stuff
                String sourcePath = fetchDir.toAbsolutePath().toString() + File.separator + stuff.getTargetPath()
                String prefixedPath
                if (pathPrefix!=null&&pathPrefix.size()>0&&pathPrefix!="null") {
                    prefixedPath = pathPrefix + '/' + ftpStuff.getFilePath()
                } else {
                    prefixedPath = ftpStuff.getFilePath()
                }
                ftp.uploadFile(Paths.get(sourcePath), prefixedPath)
            } catch (Exception e) {
                throw new FetchException(e)
            }
        }
    }

    Date getLastUpdated(Fetchable stuff) {
        sleep(10)
        if (!stuff instanceof FileFetchable) {
            throw new FetchException(new IllegalArgumentException("$stuff is not FileFetchable!"))
        } else {
            try {
                FileFetchable ftpStuff = (FileFetchable) stuff;
                String prefixedPath
                if (pathPrefix!=null&&pathPrefix.size()>0&&pathPrefix!="null") {
                    prefixedPath = pathPrefix + '/' + ftpStuff.getFilePath()
                } else {
                    prefixedPath = ftpStuff.getFilePath()
                }
                String timestamp = ftp.getTimestamp(prefixedPath)?:null
                if (timestamp == null) return null

                Date gmtDate = dateFormat.parse(timestamp)

                long gmtLong = gmtDate.getTime()

                long inLocalTime = gmtLong + TimeZone.getDefault().getOffset(gmtLong)
                return new Date(inLocalTime)
            } catch (Exception e) {
                return null
            }
        }
    }

    @Override
    public void lock() {
        lock.acquire()
    }

    @Override
    public boolean tryLock() {
        return lock.tryAcquire()
    }

    @Override
    public void unlock() {
        lock.release()
    }

    @Override
    boolean isLocked() {
        def result = lock.tryAcquire()
        if (result) lock.release()
        return result
    }

    String getDescription() {
        String res = "$username@ftp://$host:$port"
        res = pathPrefix!=null?res+"/"+pathPrefix:res
        return res
    }
}