package edu.szyrek.fetchdog.ftp

import edu.szyrek.fetchdog.FetchDogConfig
import edu.szyrek.fetchdog.FetchException
import groovy.util.logging.Slf4j
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTP

import java.nio.file.Files;
import java.nio.file.Path

@Slf4j
class FTPHelper {
    String host
    int port
    String username
    String password

    FTPHelper(String connectionHost, String username, String password, int connectionPort = 21) {
        this.username = username
        this.password = password
        this.host = connectionHost
        this.port = connectionPort
    }

    FTPClient openConnection() {
        def client = new FTPClient()
        
        try {
            client.connect(host, port)
            client.login(username, password)
            client.enterLocalPassiveMode()
            client.setFileType(FTP.BINARY_FILE_TYPE)
            return client
        } catch (IOException ex) {
            log.warn("provider ftp://$host:$port is unavaiable, reason: ${ex.getMessage()}")
            try {
                if (client.isConnected()) {
                    client.logout()
                    client.disconnect()
                }
            } catch (IOException ex2) {
                ex.printStackTrace()
            }
            throw new FetchException(ex)
        }
    }

    String getTimestamp(String filePath) {
        def client
        try {
            client = openConnection()
            return client.getModificationTime(filePath)
        } catch (IOException ex) {
            log.warn("could not get metadata for ftp://{}@{}:{}/$filePath, reason: {}",
                    username, host, port, filePath, ex.getMessage())
            throw new FetchException(ex)
        } finally {
            close(client)
        }
    }


    void mkdirs(String filePath) {
        def leftToDo = filePath
        def constructed = new StringBuffer("")
        def client
        def inTheWild = false

        try {
            client = openConnection()
            while (leftToDo.toString().size() != 0) {
                    def nextChunk
                    if (leftToDo.contains("/")) {
                        nextChunk = leftToDo.contains("/")?leftToDo.substring(0, leftToDo.indexOf("/")):leftToDo
                        leftToDo = leftToDo.substring(leftToDo.indexOf("/")+1, leftToDo.size())
                    } else {
                        nextChunk = leftToDo
                        leftToDo=""
                    }

                    constructed.append("/"+nextChunk)

                    if (!inTheWild) {
                        inTheWild = !client.changeWorkingDirectory(constructed.toString())
                    }
                    if (inTheWild) {
                        boolean mkay = client.makeDirectory(constructed.toString())
                        if (!mkay) {
                            throw new FetchException("Failed to create directory ${constructed.toString()} from ftp://${host}:${port}, reply: ${client.getReplyString()}")
                        }
                    }
            }
        } catch (IOException ex) {
            log.warn("cannot download {} from ftp://{}:{}, reason: {}", filePath, host, port, ex.getMessage())
            throw new FetchException(ex)
        } finally {
            close(client)
        }
    }

    void uploadFile(Path filePath, String targetP) {
        def client
        def inputStream
        def targetPath = targetP

        try {
            int index = targetPath.contains("/") ? targetPath.lastIndexOf("/") : 0
            String targetFTPDirectory = targetPath.substring(0,index)
            mkdirs(targetFTPDirectory)
            client = openConnection()
            log.info("Uploading {} to ftp://{}:{}/{}", filePath, host, port, targetPath)

            if (FetchDogConfig.get(FetchDogConfig.DEBUG)) {
                log.debug(" as username: {} and password: {}", username, password)
            }

            if (!Files.exists(filePath)) {
                throw new IllegalArgumentException("File: $filePath does not exist")
            }

            inputStream = new FileInputStream(filePath.toString())

            def success = client.storeFile(targetPath, inputStream)
            inputStream.close()

            if (success) {
                log.info("File {} has been uploaded successfully.", targetPath.toString())
            } else {
                throw new FetchException("Failed to upload file $filePath, got reply: ${client.getReplyString()}")
            }

        } catch (IOException ex) {
            log.warn("cannot download {} from ftp://{}:{}, reason: {}", filePath, host, port, ex.getMessage())
            throw new FetchException(ex)
        } finally {
            close(client)
            try {
                inputStream.close()
            } catch (Exception e) {
                //Ignore
            }
        }
    }

    void downloadFile(Path targetPath, String filePath) {
        def client
        def outputStream
        try {
            client = openConnection()
            log.info("Downloading {} as {} from ftp://{}:{}", filePath, targetPath, host, port)

            if (FetchDogConfig.get(FetchDogConfig.DEBUG)) {
                log.debug(" as username: {} and password: {}", username, password)
            }

            outputStream =
                    new BufferedOutputStream(
                            new FileOutputStream(
                                    new File(targetPath.toString())))
            def success = client.retrieveFile(filePath, outputStream)
            outputStream.close()

            if (success) {
                log.info("File {} has been downloaded successfully.", targetPath.toString())
            } else {
                Files.delete(targetPath)
                throw new FetchException(new FileNotFoundException("File ftp://$username@$host:$port/$filePath"))
            }

        } catch (IOException ex) {
            log.warn("cannot download {} from ftp://{}:{}, reason: {}", filePath, host, port, ex.getMessage())
            throw new FetchException(ex)
        } finally {
            close(client)
            try {
                outputStream.close()
            } catch (Exception e) {
                //Ignore
            }
        }
    }

    void close(FTPClient client) {
        try {
            if (client != null && client.isConnected()) {
                client.logout()
                client.disconnect()
            }
        } catch (IOException ex) {
            ex.printStackTrace()
        }
    }

}