package edu.szyrek.fetchdog.system

/**
 * Created by szyzyrek on 6/12/16.
 */

enum OperationSystem {
    WINDOWS, LINUX, OSX, UNDEFINED
}

enum Architecture {
    x86, x64, ppc, i386, amd64
}

class Arch {
    static final OperationSystem os = getOperationSystem()
    static final Architecture arch = getArchitecture()


    private static String getOperationSystem() {
        if (isWindows()) return OperationSystem.WINDOWS
        if (isOSX()) return OperationSystem.OSX
        if (isLinux()) return OperationSystem.LINUX
        return OperationSystem.UNDEFINED
    }
    public static String getArchitecture() {
        switch(os) {
            case OperationSystem.WINDOWS:
                return getArchWindows()
            case OperationSystem.OSX:
                return getArchOSX()
            case OperationSystem.LINUX:
                return getArchLinux()
            default:
                throw new IllegalStateException("Cannot deduce architecture for system: "+os.toString())
        }
    }

    private static Architecture getArchWindows() {
        boolean is64bit = (System.getenv("ProgramFiles(x86)") != null)
    }

    private static Architecture getArchLinux() {
        Runtime rt = Runtime.getRuntime();
        String[] commands = ["uname","-m"]
        Process proc = rt.exec(commands);

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));
        String bitsString = stdInput.readLine()
        if (bitsString.contains("64")) return Architecture.amd64
        else return Architecture.i386
    }

    private static Architecture getArchOSX() {
        Runtime rt = Runtime.getRuntime();
        String[] commands = ["uname","-m"]
        Process proc = rt.exec(commands);

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));
        String bitsString = stdInput.readLine()
        if (bitsString.contains("64")) {
            return Architecture.x64
        } else if (bitsString.toLowerCase().contains("ppc")) {
            return Architecture.ppc
        } else {
            return Architecture.i386
        }
    }

    private static boolean isWindows() {
        System.getProperty("os.name").toLowerCase().contains("win")
    }
    private static boolean isOSX() {
        System.getProperty("os.name").toLowerCase().contains("mac")
    }
    private static boolean isLinux() {
        System.getProperty("os.name").toLowerCase().contains("linux")
    }


    public static void printArch() {
        System.out.print("System info: "+os.toString()+","+arch.toString())
    }

}
