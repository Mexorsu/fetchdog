import edu.szyrek.fetchdog.ArgumentsParser;
import edu.szyrek.fetchdog.Config;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Created by bebebaba on 2016-07-14.
 */
public class ArgumentParserUT {
    Config testConfig;
    ArgumentsParser testParser;
    private final static String TEST_GITIGNORE_LOCATION = "user.home"+ File.separator+".gitignore";

    @BeforeTest
    void cleanTestConfig() {
        this.testConfig = new Config();
        this.testParser = new ArgumentsParser(testConfig);
    }

    @Test
    void testGitignoreArg() {
        Assert.assertNull(testConfig.get(Config.LABEL_GITIGNORE_LOCATION, null));
        testParser.parseCommandLineArgs(new String[]{"-G "+System.getProperty(TEST_GITIGNORE_LOCATION)});
        Assert.assertEquals(testConfig.get(Config.LABEL_GITIGNORE_LOCATION), TEST_GITIGNORE_LOCATION);
    }

    @Test
    void testVerboseFlag() {
        Assert.assertEquals(testConfig.get(Config.LABEL_VERBOSE, false), false);
        testParser.parseCommandLineArgs(new String[]{"-v"});
        Assert.assertEquals(testConfig.get(Config.LABEL_VERBOSE), true);
    }
}
